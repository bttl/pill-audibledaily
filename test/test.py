#!/usr/bin/env python
import sys
import os
location = ''.join([os.getcwd(), '/'])
sys.path.append(location)
import audibledaily as ad
data = ad.checkDeal(os.environ['ADUSER'],
                    os.environ['ADPASS'],
                    os.environ['POPSERV'])
alldata = ad.processData(data, loc=location, filename='audible.data')
ad.writeJSON(data, loc=location, filename='audible.json')
ad.writeRSS(alldata, loc=location, filename='audible.rss')
ad.writeXML(data, loc=location, filename='audible.xml')

data = ad.checkDeal(os.environ['ADUSER'],
                    os.environ['ADPASS'],
                    os.environ['POPSERV'])
alldata = ad.processData(data, loc=location, filename='audible.data')
ad.writeJSON(data, loc=location, filename='audible.json')
ad.writeRSS(alldata, loc=location, filename='audible.rss')
ad.writeXML(data, loc=location, filename='audible.xml')

# Check  filesize to make sure they aren't empty
jsonsize = os.path.getsize(''.join([location, 'audible.json']))
xmlsize = os.path.getsize(''.join([location, 'audible.xml']))

if jsonsize < 5:
  raise Exception("JSON is empty...")

if xmlsize < 125:
  raise Exception("XML is empty...")
