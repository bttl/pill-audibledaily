"""Audible's Daily Deals
http://bitbucket.org/bttl/pill-audibledaily.git

Audible's Daily Deals grabs the latest daily deal from Audible.com and
presents the data in an easy to access manner.

Dependencies include:
  lxml>=3.2.3

Current outputs include:
  JSON        -> writeJSON()
  RSS 2.0     -> writeRSS()
  XML 1.0     -> writeXML()

License:

Copyright (c) 2012-2013, Trae Blain
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the names of Trae Blain nor BoTTLe Tools nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""
import os
import poplib
import re
import json
from email import parser
from datetime import datetime as dt
from datetime import timedelta
from lxml import etree


def checkDeal(user, passwd, pop):
  """Primary function.  Used to fetch the latest daily deal from an email
  account that supports POP3 over SSL.  It should work with a personal email
  address as it skips emails not from <newsletters@audible.com>.  If you have
  a large inbox it may hang.

  Inputs include:
    - Email Username: user
    - Email Password: passwd
    - Email Server: pop

  Output is a dict of the latest Audible deal.
  """
  popcon = poplib.POP3_SSL(pop)
  popcon.user(user)
  popcon.pass_(passwd)

  messages = [popcon.retr(i) for i in range(1, len(popcon.list()[1]) + 1)]
  popcon.quit()

  messages = ["\n".join(mssg[1]) for mssg in messages]
  mssgs = [parser.Parser().parsestr(mssg) for mssg in messages]

  for mess in reversed(mssgs):
    if '<newsletters@audible.com>' not in mess['From']:
      continue
    for part in mess.walk():
      if 'text/plain' in part.get_content_type():
        details = part.as_string()
      if 'text/html' in part.get_content_type():
        regex = re.compile("\"(http://ecx.images-amazon.com/.+?)\"",
                           re.MULTILINE)
        r = regex.search(part.as_string())
        try:
          img = r.group(1)
        except:
          img = ''
    data = {'date': dt.strptime(mess['Date'], "%a, %d %b %Y %H:%M:%S +0000"),
            'image': img}
    break

  regex = re.compile("[_]+\s?\n", re.MULTILINE)
  body = details.split(regex.findall(details)[0])[1]

  regex = re.compile("(https://.+?)\n", re.MULTILINE)
  r = regex.search(body)
  data['url'] = r.group(1)
  data['id'] = data['url'].split('%3D')[-1]

  regex = re.compile("\n\n((.+)\sby\s(.+))\n\n", re.MULTILINE)
  r = regex.search(body)
  data['title'] = r.group(2)
  data['author'] = r.group(3)

  regex = re.compile("(\$[0-9.]+)", re.MULTILINE)
  r = regex.search(body)
  data['price'] = r.group(1)

  splitbody = body.split("\n\n")
  data['content'] = [s for s in splitbody if len(s) == max(map(len,
                                                               splitbody))][0]

  return data


def processData(data, loc='./', filename='data.json'):
  """Processes the data.  The script uses flat-file data storage, therefore it
  pulls the data from a file (Default: data.json), verifies it isn't just a
  re-check of old information.  If it is a new deal, it is added to the data
  store.  It also prunes the old data (older than 7 days) from the data store
  for size.

  Inputs are:
    - Data taken from checkDeal() function. (dict)
    - Location of data storage: loc='./' (Default)
    - Name of the data file: filename='data.josn' (Default)

  Output is the full list of stored data in a single dict.
  """
  if not os.path.exists(os.path.join(os.path.abspath(loc), filename)):
    newFile = open(os.path.join(os.path.abspath(loc), filename), 'w')
    newFile.write('{}')
    newFile.close()

  existing = open(os.path.join(os.path.abspath(loc), filename), 'r')
  allData = json.loads(existing.read())
  existing.close()

  if data['id'] in allData:
    return allData

  toDelete = []
  for i in allData:
    itemdate = dt.strptime(allData[i]['date'],
                           '%a, %d %b %Y %H:%M:%S +0000')
    if itemdate < dt.utcnow() - timedelta(days=7):
      toDelete.append(i)
  for i in toDelete:
    del allData[i]

  allData[data['id']] = {
    'title': data['title'],
    'author': data['author'],
    'details': data['content'],
    'price': data['price'],
    'url': data['url'],
    'image': data['image'],
    'date': dt.strftime(data['date'], '%a, %d %b %Y %H:%M:%S +0000')
  }

  newFile = open(os.path.join(os.path.abspath(loc), filename), 'w')
  newFile.write(json.dumps(allData, sort_keys=True, indent=2))
  newFile.close()

  return allData


def writeJSON(data, loc='./', filename='audibledaily.json'):
  """Writes JSON Data.  This contains only today's deal data.

  Inputs are:
    - Data from checkDeal() (dict)
    - Location of data storage (loc="./" Default)
    - Data file name (audibledaily.json Default)

  Output is JSON file in the location specified.
  """
  writeData = {}
  writeData[data['id']] = {
    'title': data['title'],
    'author': data['author'],
    'details': data['content'],
    'price': data['price'],
    'url': data['url'],
    'image': data['image'],
    'date': dt.strftime(data['date'], '%a, %d %b %Y %H:%M:%S +0000')
  }

  if data:
    wfile = open(os.path.join(os.path.abspath(loc), filename), 'w')
    wfile.write(json.dumps(writeData, sort_keys=True, indent=2))
    wfile.close()
  return


def writeRSS(data, loc='./', filename='audilbledaily.rss'):
  """Write RSS 2.0 data.  This writes the latest 7 days of deals, sorted by
  date, truncating any additional data.

  Inputs are:
    - All data from processData() function. (dict)
    - Location of RSS file. (Default "./")
    - Filename to store passed RSS data. (Default "audibledaily.rss")

  Output is RSS file in location specified.
  """
  d = sorted(data.items(),
             key=lambda x: dt.strptime(x[1]['date'],
                                       '%a, %d %b %Y %H:%M:%S +0000'),
             reverse=True)
  now = dt.utcnow().strftime("%a, %d %b %Y %H:%M:%S +0000")

  tree = etree.Element('rss', version="2.0")
  channel = etree.SubElement(tree, 'channel')
  etree.SubElement(channel, 'title').text = "Audible Daily Deal"
  etree.SubElement(channel, 'link').text = "http://bt.tl/pill/audible/"
  etree.SubElement(channel, 'description').text = "Feed of Audible's Daily \
Deal brought to you by Trae Blain.  http://traebla.in/ http://bt.tl/"
  etree.SubElement(channel, 'language').text = "en-us"
  etree.SubElement(channel, 'pubDate').text = now
  etree.SubElement(channel, 'lastBuildDate').text = now
  etree.SubElement(channel, 'docs').text = "http://bt.tl/pill/audible/"
  etree.SubElement(channel, 'managingEditor').text = "trae@traeblain.com"
  etree.SubElement(channel, 'webMaster').text = "trae@traeblain.com"

  for item in d[:16]:
    imglink = '<img src="{0}" style="float: left; margin-right: 20px;" />\
'.format(item[1]['image'])
    subItem = etree.SubElement(channel, 'item')
    etree.SubElement(subItem, 'title').text = item[1]['title']
    etree.SubElement(subItem, 'author').text = item[1]['author']
    etree.SubElement(subItem, 'link').text = item[1]['url']
    etree.SubElement(subItem, 'description').text = etree.CDATA(
                                                  '<br>'.join([
                                                    imglink,
                                                    item[1]['details'],
                                                    'Get it Today Only for:',
                                                    item[1]['price'],
                                                    '<br>']))
    etree.SubElement(subItem, 'pubDate').text = item[1]['date']
    etree.SubElement(subItem, 'guid').text = item[0]

  wfile = open(os.path.join(os.path.abspath(loc), filename), 'w')
  wfile.write(etree.tostring(tree,
                             pretty_print=True,
                             xml_declaration=True,
                             encoding='utf-8'))
  wfile.close()
  return


def writeXML(data, loc='./', filename='audibledaily.xml'):
  """Writes XML Data.  This contains only today's deal data.

  Inputs are:
    - Data from checkDeal() (dict)
    - Location of data storage (loc="./" Default)
    - Data file name (audibledaily.xml Default)

  Output is XML file in the location specified.
  """
  if not data:
    return

  writeData = {}
  writeData[data['id']] = {
    'title': data['title'],
    'author': data['author'],
    'details': data['content'],
    'price': data['price'],
    'url': data['url'],
    'image': data['image'],
    'date': dt.strftime(data['date'], '%a, %d %b %Y %H:%M:%S +0000')
  }

  tree = etree.Element('audible')

  for i in writeData:
    subItem = etree.SubElement(tree, 'item')
    etree.SubElement(subItem, 'id').text = i
    etree.SubElement(subItem, 'title').text = writeData[i]['title']
    etree.SubElement(subItem, 'author').text = writeData[i]['author']
    etree.SubElement(subItem, 'link').text = writeData[i]['url']
    etree.SubElement(subItem, 'price').text = writeData[i]['price']
    etree.SubElement(subItem, 'image').text = writeData[i]['image']
    etree.SubElement(subItem, 'description').text = etree.CDATA(
                                                  writeData[i]['details'])
    etree.SubElement(subItem, 'pubDate').text = writeData[i]['date']
  tree.append(etree.Comment("Brought to you by http://traebla.in/ http://bt.tl\
/"))

  wfile = open(os.path.join(os.path.abspath(loc), filename), 'w')
  wfile.write(etree.tostring(tree,
                             pretty_print=True,
                             xml_declaration=True,
                             encoding='utf-8'))
  wfile.close()
  return
