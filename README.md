# Audible Daily Deals

This is a basic library to pull the latest Audible Daily Deals from their
emails and present them in an easy to aggregate format. Storage is handled
using a [JSON][] file only.  No database required.  Simple, readable, awesomle.

Working product: `Coming Soon`

[ ![Codeship Status for bttl/pill-audibledaily](https://www.codeship.io/projects/7e1c7800-3f85-0131-c536-0e9a90f6062f/status?branch=master)](https://www.codeship.io/projects/10462)

## Usage:

```python
>>> import audibledaily as ad
>>> username = 'myemail@email.com'
>>> password = 'mypassword'
>>> server = 'popemail.server.com'
>>> data = ad.checkDeal(username, password, server)
>>> alldata = ad.processData(data, loc='./', filename='data.json')
>>> ad.writeJSON(data, loc='./', filename='audible.json')
>>> ad.writeRSS(alldata, loc='./', filename='audible.rss')
>>> ad.writeXML(data, loc='./', filename='audible.xml')
```

### `checkDeal(user, passwd, server)`

Acquires the data from a POP3 over SSL compatible server. Returns a dictionary
object.

### `processData(data, loc='./', filename='data.json')`

Organizes everything taken from the checkDeal function.  Writes to `data.json`
and returns a dictionary object of the past seven days of deals.

### `writeJSON(data, loc='./', filename='audible.json')`

Writes your output JSON file for use in your tool.

### `writeRSS(data, loc='./', filename='audible.rss')`

Writes your output [RSS 2.0][] file for use in your tool|aggregator.

### `writeXML(data, loc='./', filename='audible.xml')`

Writes your output [XML][] file for use in your tool.

## TODO

- Add more output types
    - RSS 0.91, 0.92 _(Maybe...)_
    - Atom
    - CSV _(Maybe...)_
- Use Firebase or Database store (Maybe...)


[RSS 2.0]: http://cyber.law.harvard.edu/rss/rss.html
[JSON]: http://www.json.org/
[XML]: http://www.w3.org/TR/REC-xml/
[ATOM]: http://www.atomenabled.org/developers/syndication/atom-format-spec.php
